#!/usr/bin/python

from PIL import Image
import sys
import math


def main():
    if sys.argv[1] == 'encode':
        if sys.argv[2] == 'text':
            encode_message(sys.argv[3], sys.argv[4])
        elif sys.argv[2] == 'file':
            try:
                secret_file = open(sys.argv[4], 'rb')
                secret = secret_file.read()
            except:
                print(
                    sys.argv[4] + ' was not found. Dont forget the file extension.')
                exit()
            encode_message(sys.argv[3], str(secret))

    elif sys.argv[1] == 'decode':
        decode_message(sys.argv[2])


# get secret message.
def decode_message(image_name):
    """ 
        open encoded image and traverse color channels
        convert integers to bits (255 to 11111111).
        add the bits to string and so on.
        traverse string by increments of 8 ( for characters ).
        trim excess characters and return.
    """
    secret = []
    secret_bits = ""
    try:
        image = Image.open(image_name)
    except:
        print('Image not found.')
        exit()
    image_pixels = image.getdata()
    size = ""
    try:
        for i in range(0, 10):
            red = decimal_to_byte(image_pixels[i][0])[6:8]
            green = decimal_to_byte(image_pixels[i][1])[6:8]
            blue = decimal_to_byte(image_pixels[i][2])[6:8]
            size += red + green + blue
        size = byte_to_decimal(size)
        for i in range(10, 10+size+math.ceil(size/3)):
            red = decimal_to_byte(image_pixels[i][0])[6:8]
            green = decimal_to_byte(image_pixels[i][1])[6:8]
            blue = decimal_to_byte(image_pixels[i][2])[6:8]
            secret_bits += red + green + blue
        for i in range(0, len(secret_bits) + len(secret_bits) % 8, 8):
            secret.append(chr(byte_to_decimal(secret_bits[i:i+8])))
        print(''.join(secret[0:size]))
        return ''.join(secret[0:size])
    except:
        print('Image could not be decoded.')
        exit()


# hide string in picture.
def encode_message(image_name, string_ascii):
    """
        open image to be encoded.
        convert string to bits.
        get image pixel data in a 1D array.
        goto get_new_pixels to get new encoded pixel data.
        put new pixels into existing image.
        save image.
    """
    try:
        image = Image.open(image_name)
    except:
        print('Image not found.')
        exit()
    string_bits = string_to_bits(string_ascii)
    try:
        image_pixels = image.getdata()
    except:
        print('Image data could not be accessed. Image was not found.')
        exit()
    new_pixels = get_new_pixels(image_pixels, string_bits)
    image_reconstruction(new_pixels, image)
    image.save('encoded.png', 'PNG')


# add new pixels into photo.
def image_reconstruction(new_pixels, image):
    image.putdata(new_pixels)


# modify last 2 bits in pixel values(r,g,b) append the result to new list. return result.
def get_new_pixels(image_pixels, string_bits):
    """
        first for loop to encode size of string into first 10 pixels 
        ( using the last 2 bits to encode so we will end up with 2^20 - 1 )
        second for loop to encode message
        add new pixels to array.
        add leftovers of message to the next pixel channel 
        while adding the other channels from original.
        return new encoded pixels.
    """
    l = 0
    k = 0
    new_bytes = []
    size = '{0:060b}'.format(len(string_bits)//8)
    try:
        for i in range(0, len(size), 2):
            image_byte = decimal_to_byte(image_pixels[l][k])
            string_bit = size[i:i+2]
            new_image_byte = image_byte[0:6] + string_bit
            new_bytes.append(byte_to_decimal(new_image_byte))
            k += 1
            if k % 3 == 0:
                l += 1
            k %= 3
        for i in range(0, len(string_bits), 2):
            image_byte = decimal_to_byte(image_pixels[l][k])
            string_bit = string_bits[i:i+2]
            new_image_byte = image_byte[0:6] + string_bit
            new_bytes.append(byte_to_decimal(new_image_byte))
            k += 1
            if k % 3 == 0:
                l += 1
            k %= 3
        new_bytes_tuple = []
        for i in range(0, len(new_bytes) - len(new_bytes) % 3, 3):
            new_bytes_tuple.append(
                (new_bytes[i], new_bytes[i+1], new_bytes[i+2]))
        leftovers = []
        for i in range(len(new_bytes_tuple)*3, len(new_bytes)):
            leftovers.append(new_bytes[i])
        for i in range(len(leftovers), 3):
            leftovers.append(image_pixels[len(new_bytes_tuple)][i])
        new_bytes_tuple.append(tuple(leftovers))

        return new_bytes_tuple
    except:
        print('Image too small to encode message.')


# convert integer to binary string.
def decimal_to_byte(num):
    return '{0:08b}'.format(num)


# convert string bits to integer.
def byte_to_decimal(byte):
    decimal = 0
    byte = byte[::-1]
    for i in range(len(byte)):
        decimal += int(byte[i]) * (2 ** i)
    return decimal


# string to bytes.
def string_to_bits(string_ascii):
    string_ascii = str.encode(string_ascii)
    string_bits_array = []
    for i in range(len(string_ascii)):
        string_bits_array.append(decimal_to_byte(string_ascii[i]))
    return ''.join(string_bits_array)


if __name__ == '__main__':
    main()
